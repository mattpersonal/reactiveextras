#
# Be sure to run `pod lib lint ReactiveExtras.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'ReactiveExtras'
s.version          = '0.0.14'
s.summary          = 'Framework providing components that support reactive programming.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
'Framework providing reactive components that support an event-driven app architecture.'
DESC

s.homepage         = 'https://bitbucket.org/mattpersonal/reactiveextras'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'mrrichardson52' => 'mattyrrich52@gmail.com' }
s.source           = { :git => 'https://mrrichardson52@bitbucket.org/mattpersonal/reactiveextras.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '13.0'

s.source_files = 'ReactiveExtras/Sources/**/*'
s.preserve_paths = [ 'ReactiveExtras/SomeJson.json' ]

s.swift_version= '4.2'

# s.resource_bundles = {
#   'ReactiveExtras' => ['ReactiveExtras/Assets/*.png']
# }

# s.public_header_files = 'Pod/Classes/**/*.h'
# s.frameworks = 'UIKit', 'MapKit'
# s.dependency 'AFNetworking', '~> 2.3'

end
