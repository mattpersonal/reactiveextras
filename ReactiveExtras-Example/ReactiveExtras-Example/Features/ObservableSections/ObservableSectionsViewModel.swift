//
//  ObservableSectionsViewModel.swift
//  ReactiveExtras-Example
//
//  Created by Matthew Richardson on 9/14/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import ReactiveExtras

class ObservableSectionsViewModel {
    
    enum Section {
        case strings
        case numbers
    }
    
    // String section
    private let stringRows = ObservableArray<String>()
    lazy var stringSection = ObservableSection(observableRowArray: stringRows)
    
    // Number section
    private let numberRows = ObservableArray<Int>()
    lazy var numberSection = ObservableSection(observableRowArray: numberRows)
    
    init() {
        updateTable()
    }
    
    func updateTable() {
        stringRows.update(["String 1", "String 2", "String 3"])
        numberRows.update([0, 1, 2])
    }
    
    func updateSectionMetadata(section: Section) {
        switch section {
        case .strings:
            stringSection.metadata = SectionMetadata(headerTitle: "Updated String Header Title",
                                                     footerTitle: "Updated String Footer Title")
            
        case .numbers:
            numberSection.metadata = SectionMetadata(headerTitle: "Updated Number Header Title",
                                                     footerTitle: "Updated Number Footer Title")
        }
    }
    
    func insertRow(_ row: Int, section: Section) {
        switch section {
        case .strings:
            stringRows.insert("Insert String \(row)", at: row)
            
        case .numbers:
            numberRows.insert(999, at: row)
        }
    }
    
    func removeRow(_ row: Int, section: Section) {
        switch section {
        case .strings:
            stringRows.remove(at: row)
            
        case .numbers:
            numberRows.remove(at: row)
        }
    }
    
    func replaceRow(_ row: Int, section: Section) {
        switch section {
        case .strings:
            stringRows[row] = "Replaced String \(row)"
            
        case .numbers:
            numberRows[row] = 999
        }
    }
}

