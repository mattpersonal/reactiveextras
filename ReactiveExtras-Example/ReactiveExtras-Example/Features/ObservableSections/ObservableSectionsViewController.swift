//
//  ObservableSectionsViewController.swift
//  ReactiveExtras-Example
//
//  Created by Matthew Richardson on 9/14/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import ReactiveExtras
import ReactiveSwift

class ObservableSectionsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var rowTextField: UITextField!
    
    // action buttons
    @IBOutlet weak var updateTableButton: UIButton!
    @IBOutlet weak var updateStringSectionButton: UIButton!
    @IBOutlet weak var insertStringRowButton: UIButton!
    @IBOutlet weak var removeStringRowButton: UIButton!
    @IBOutlet weak var replaceStringRowButton: UIButton!
    @IBOutlet weak var updateNumberSectionButton: UIButton!
    @IBOutlet weak var insertNumberRowButton: UIButton!
    @IBOutlet weak var removeNumberRowButton: UIButton!
    @IBOutlet weak var replaceNumberRowButton: UIButton!

    private let defaultRow = 0
    private var currentRow: Int {
        guard let rowText = rowTextField.text else {
            return defaultRow
        }
        
        return Int(rowText) ?? defaultRow
    }
    
    private let viewModel = ObservableSectionsViewModel()
    
    private lazy var reactiveDataSource: ObservableSectionsReactiveTableViewDataSource = {
        let sectionDataSources = [
            stringSectionDataSource,
            numberSectionDataSource
        ]
        return ObservableSectionsReactiveTableViewDataSource(tableView: self.tableView,
                                                             reactiveSectionDataSources: sectionDataSources)
    }()
    
    private lazy var stringSectionDataSource: ReactiveTableViewSectionDataSource = {
        return ReactiveTableViewSectionDataSource.Builder(observableSection: viewModel.stringSection)
            .onCreateRowCell { [weak self] (indexPath, _, tableView) -> UITableViewCell? in
                guard let strongSelf = self else {
                    return nil
                }
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "Table View Cell", for: indexPath)
                cell.textLabel?.text = strongSelf.viewModel.stringSection.observableRowArray[indexPath.row]
                return cell
            }
            .build()
    }()
    
    private lazy var numberSectionDataSource: ReactiveTableViewSectionDataSource = {
        return ReactiveTableViewSectionDataSource.Builder(observableSection: viewModel.numberSection)
            .onCreateRowCell { [weak self] (indexPath, _, tableView) -> UITableViewCell? in
                guard let strongSelf = self else {
                    return nil
                }
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "Table View Cell", for: indexPath)
                cell.textLabel?.text = String(strongSelf.viewModel.numberSection.observableRowArray[indexPath.row])
                return cell
            }
            .build()
    }()
    
    static func instantiate() -> ObservableSectionsViewController {
        let storyboard = UIStoryboard(name: "ObservableSections", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ObservableSectionsViewController") as! ObservableSectionsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Observable Sections"
        
        setupDismissKeyboardHandling()
        setupTextFields()
        setupTableView()
        observeButtonTaps()
    }
    
    private func setupDismissKeyboardHandling() {
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func setupTextFields() {
        rowTextField.placeholder = "Default `0`"
    }
    
    private func setupTableView() {
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Table View Cell")
        tableView.dataSource = reactiveDataSource
    }
    
    private func observeButtonTaps() {
        updateTableButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.updateTable()
        }
        
        updateStringSectionButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.updateSectionMetadata(section: .strings)
        }
        
        insertStringRowButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.insertRow(strongSelf.currentRow, section: .strings)
        }
        
        removeStringRowButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.removeRow(strongSelf.currentRow, section: .strings)
        }
        
        replaceStringRowButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.replaceRow(strongSelf.currentRow, section: .strings)
        }
        
        updateNumberSectionButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.updateSectionMetadata(section: .numbers)
        }
        
        insertNumberRowButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.insertRow(strongSelf.currentRow, section: .numbers)
        }
        
        removeNumberRowButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.removeRow(strongSelf.currentRow, section: .numbers)
        }
        
        replaceNumberRowButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.replaceRow(strongSelf.currentRow, section: .numbers)
        }
    }
}
