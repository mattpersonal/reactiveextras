//
//  ObservableTableViewModel.swift
//  ReactiveExtras-Example
//
//  Created by Matthew Richardson on 9/13/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import ReactiveExtras

class ObservableTableViewModel {
    
    /// Table storing the data for the table
    let observableTable = ObservableTable()
    
    /// Type of section
    typealias ExampleSection = ObservableSection<String>
    
    init() {
        // nothing needed on initialization
    }
    
    func updateTable() {
        let rowArray = ObservableArray(elements: ["Update Table - Row 0", "Update Table - Row 1", "Update Table - Row 2"])
        let sectionMetadata = SectionMetadata(headerTitle: "Update Table - Section Header 0", footerTitle: "Update Table - Section Footer 0")
        let sectionForUpdate = ObservableSection(observableRowArray: rowArray, metadata: sectionMetadata)
        
        observableTable.update(sections: [sectionForUpdate])
    }
    
    func insertSection(at index: Int) {
        let rowArray = ObservableArray(elements: ["Insert Section - 0", "Insert Section - 1", "Insert Section - 2"])
        let sectionMetadata = SectionMetadata(headerTitle: "Insert Section - Section \(index)", footerTitle: "Insert Section - Section \(index)")
        let newSection = ObservableSection(observableRowArray: rowArray, metadata: sectionMetadata)
        
        observableTable.insert(section: newSection, at: index)
    }
    
    func removeSection(at index: Int) {
        observableTable.remove(sectionAt: index)
    }
    
    func updateSectionMetadata(at index: Int) {
        guard let exampleSection = observableTable.section(at: index) as? ExampleSection else {
            fatalError("Cast to Example Section failed.")
        }
        
        exampleSection.metadata = SectionMetadata(headerTitle: "Updated Section Metadata - Section \(index)",
                                                  footerTitle: "Update Section Metadata - Section \(index)")
    }
    
    func insertRow(at indexPath: IndexPath) {
        guard let exampleSection = observableTable.section(at: indexPath.section) as? ExampleSection else {
            fatalError("Cast to Example Section failed.")
        }
        
        exampleSection.observableRowArray.insert("Insert Row - Section \(indexPath.section), Row \(indexPath.row)", at: indexPath.row)
    }
    
    func removeRow(at indexPath: IndexPath) {
        guard let exampleSection = observableTable.section(at: indexPath.section) as? ExampleSection else {
            fatalError("Cast to Example Section failed.")
        }
        
        exampleSection.observableRowArray.remove(at: indexPath.row)
    }
    
    func replaceRow(at indexPath: IndexPath) {
        guard let exampleSection = observableTable.section(at: indexPath.section) as? ExampleSection else {
            fatalError("Cast to Example Section failed.")
        }
        
        exampleSection.observableRowArray[indexPath.row] = "Replaced Row - Section \(indexPath.section), Row \(indexPath.row)"
    }
}
