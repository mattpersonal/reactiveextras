//
//  ObservableTableViewController.swift
//  ReactiveExtras-Example
//
//  Created by Matthew Richardson on 9/12/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import ReactiveExtras

class ObservableTableViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    // section/row text fields
    @IBOutlet weak var sectionTextField: UITextField!
    @IBOutlet weak var rowTextField: UITextField!
    
    // action buttons
    @IBOutlet weak var updateTableButton: UIButton!
    @IBOutlet weak var insertSectionButton: UIButton!
    @IBOutlet weak var removeSectionButton: UIButton!
    @IBOutlet weak var updateSectionMetadataButton: UIButton!
    @IBOutlet weak var insertRowButton: UIButton!
    @IBOutlet weak var removeRowButton: UIButton!
    @IBOutlet weak var replaceRowButton: UIButton!
    
    private let defaultSection = 0
    private var currentSection: Int {
        guard let sectionText = sectionTextField.text else {
            return defaultSection
        }
        
        return Int(sectionText) ?? defaultSection
    }
    
    private let defaultRow = 0
    private var currentRow: Int {
        guard let rowText = rowTextField.text else {
            return defaultRow
        }
        
        return Int(rowText) ?? defaultRow
    }
    
    private var currentIndexPath: IndexPath {
        return IndexPath(row: currentRow, section: currentSection)
    }
    
    private let viewModel = ObservableTableViewModel()
    
    private var reactiveDataSource: ObservableTableReactiveTableViewDataSource?
    
    static func instantiate() -> ObservableTableViewController {
        let storyboard = UIStoryboard(name: "ObservableTable", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ObservableTableViewController") as! ObservableTableViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Observable Table"
        
        setupDismissKeyboardHandling()
        setupTextFields()
        setupTableView()
        observeButtonTaps()
    }
    
    private func setupDismissKeyboardHandling() {
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func setupTextFields() {
        sectionTextField.placeholder = "Default `0`"
        rowTextField.placeholder = "Default `0`"
    }
    
    private func setupTableView() {
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Table View Cell")
        
        reactiveDataSource =
            ObservableTableReactiveTableViewDataSource.Builder(tableView: tableView,
                                                               observableTable: viewModel.observableTable)
                .onCreateRowCell { (tableView, indexPath, observableTable) -> UITableViewCell? in
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Table View Cell", for: indexPath)
                    
                    guard let section = observableTable.section(at: indexPath.section) as? ObservableTableViewModel.ExampleSection else {
                        fatalError("Failed conversion to ExampleSection")
                    }
                    
                    cell.textLabel?.text = section.observableRowArray[indexPath.row]
                    return cell
                }
                .build()
        
        tableView.dataSource = reactiveDataSource
    }
    
    private func observeButtonTaps() {
        updateTableButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.updateTable()
            }
        
        insertSectionButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.insertSection(at: strongSelf.currentSection)
            }
        
        removeSectionButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.removeSection(at: strongSelf.currentSection)
            }
        
        updateSectionMetadataButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.updateSectionMetadata(at: strongSelf.currentSection)
            }
        
        insertRowButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.insertRow(at: strongSelf.currentIndexPath)
            }
        
        removeRowButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.removeRow(at: strongSelf.currentIndexPath)
            }
        
        replaceRowButton.reactive.controlEvents(.touchUpInside)
            .observeValues { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.viewModel.replaceRow(at: strongSelf.currentIndexPath)
            }
    }
}
