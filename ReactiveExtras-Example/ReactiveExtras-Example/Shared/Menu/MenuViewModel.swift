//
//  MenuViewModel.swift
//  RichKit-Example
//
//  Created by Matthew Richardson on 6/13/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

/// View model for a menu.
protocol MenuViewModel {
    
    /// Title of the menu
    var menuTitle: String? { get }
    
    /// Menu items
    var menuItems: [MenuItem] { get }
    
}
