//
//  MenuViewController.swift
//  RichKit-Example
//
//  Created by Matthew Richardson on 6/12/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

/// Basic menu view controller that will link out to other menus or example/text features
final class MenuViewController: UIViewController {
    
    /// Table view showing the links to each of the example features.
    private let menuItemsTableView = UITableView()
    
    /// View model for the Menu
    private let viewModel: MenuViewModel
    
    init(viewModel: MenuViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        fatalError("Don't initialize using nib.")
    }
    
    required init?(coder: NSCoder) {
        fatalError("Don't initialize in storyboard.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupTableView()
        updateView()
    }
    
    private func setupTableView() {
        menuItemsTableView.translatesAutoresizingMaskIntoConstraints = false
        view.fill(with: menuItemsTableView)
        
        menuItemsTableView.dataSource = self
        menuItemsTableView.delegate = self
        
        menuItemsTableView.tableFooterView = UIView(frame: .zero)
        
        menuItemsTableView.allowsMultipleSelection = false
        menuItemsTableView.allowsSelection = true
        
        menuItemsTableView.rowHeight = UITableView.automaticDimension
        menuItemsTableView.estimatedRowHeight = 100
        
        menuItemsTableView.register(MenuItemTableViewCell.self, forCellReuseIdentifier: MenuItemTableViewCell.reuseIdentifier)
    }
    
    private func updateView() {
        guard isViewLoaded else { return }
        
        title = viewModel.menuTitle
        menuItemsTableView.reloadData()
    }
    
}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: MenuItemTableViewCell.reuseIdentifier, for: indexPath) as? MenuItemTableViewCell else {
            return UITableViewCell()
        }
        
        cell.setItem(viewModel.menuItems[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = viewModel.menuItems[indexPath.row]
        let itemViewController = item.viewControllerForItem()
        navigationController?.pushViewController(itemViewController, animated: true)
    }
    
}
