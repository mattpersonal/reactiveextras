//
//  MenuItemTableViewCell.swift
//  RichKit-Example
//
//  Created by Matthew Richardson on 6/12/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {

    @IBOutlet private var rootView: UIView!
    @IBOutlet private weak var mainLabel: UILabel!
    
    static let reuseIdentifier = "MenuItemTableViewCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("MenuItemTableViewCell", owner: self, options: nil)
        fill(with: rootView, margin: 8)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        // leave empty so that nothing happens
    }
    
    /// Populates the cell using the Menu Item
    ///
    /// - Parameter item: Menu Item used to populate the cell
    func setItem(_ item: MenuItem) {
        mainLabel.text = item.displayName
    }
}
