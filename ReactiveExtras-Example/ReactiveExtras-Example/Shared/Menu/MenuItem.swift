//
//  MenuItem.swift
//  RichKit-Example
//
//  Created by Matthew Richardson on 6/13/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

/// Protocol for a menu item that launches to an example/test feature or another menu. 
protocol MenuItem {
    
    /// Display name for the menu item
    var displayName: String { get }
    
    /// Constructs a view controller for the item
    func viewControllerForItem() -> UIViewController
    
}
