//
//  UIView.swift
//  ReactiveExtrasExample
//
//  Created by Matthew Richardson on 9/12/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Adds the parameter view to this view using autolayout. Pins the subview to all edges of this view.
    ///
    /// - Parameter view: view to add as subview
    func fill(with view: UIView?) {
        fill(with: view, margin: 0)
    }
    
    /// Adds the parameter view to this view with margins using autolayout.
    ///
    /// - Parameters:
    ///   - view: view to add as subview
    ///   - margin: space between subview and this view on all sides
    func fill(with view: UIView?, margin: CGFloat) {
        guard let view = view else { return }
        
        // enable autolayout
        view.translatesAutoresizingMaskIntoConstraints = false
        
        // add view
        addSubview(view)
        
        // constrain the view
        view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: margin).isActive = true
        view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -margin).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor, constant: margin).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -margin).isActive = true
    }
}
