//
//  MainMenuViewModel.swift
//  RichKit-Example
//
//  Created by Matthew Richardson on 6/13/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

class MainMenuViewModel: MenuViewModel {
    
    /// The Features that can be linked to from the Main Menu
    enum Feature: CaseIterable, MenuItem {
        
        /// Feature displaying the observable table functionality
        case observableTable
        
        /// Feature displaying the observable sections functionality
        case observableSections
        
        /// The item name as it will display in the menu
        var displayName: String {
            switch self {
            case .observableTable:
                return "Observable Table - Reactive Table View"
                
            case .observableSections:
                return "Observable Sections - Reactive Table View"
            }
        }
        
        /// The view controller that will be pushed when the menu item is selected.
        ///
        /// - Returns: view controller for the menu item
        func viewControllerForItem() -> UIViewController {
            switch self {
            case .observableTable:
                return ObservableTableViewController.instantiate()
                
            case .observableSections:
                return ObservableSectionsViewController.instantiate()
            }
        }
    }
    
    var menuTitle: String? {
        return "Main Menu"
    }
    
    var menuItems: [MenuItem] {
        return Feature.allCases
    }
}
