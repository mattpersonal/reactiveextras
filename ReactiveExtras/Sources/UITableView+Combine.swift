//
//  UITableView+Combine.swift
//  ReactiveExtras
//
//  Created by Matthew Richardson on 10/23/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine

// MARK: - TableOperation

/// All possible operations that can be performed on the data in a table
public enum TableOperation {
    
    /// The entire table was updated
    case reloadTable(updatedSectionCount: Int)
    
    /// Sections were inserted into the table
    case insertSections(sections: IndexSet)
    
    /// Sections were deleted from the table
    case deleteSections(sections: IndexSet)
    
    /// Data in some sections was changed
    case reloadSections(infos: Set<ReloadSectionInfo>)
    
    /// Rows were inserted into sections in the table
    case insertRows(rows: Set<IndexPath>)
    
    /// Rows were deleted from sections in the table
    case deleteRows(rows: Set<IndexPath>)
    
    /// Some rows were deleted from sections and were replaced with new rows
    /// - Note: deleted rows are processed before the inserted rows
    case replaceRows(deletedRows: Set<IndexPath>, insertedRows: Set<IndexPath>)
    
    /// A row was moved to new location in the table
    case moveRow(source: IndexPath, destination: IndexPath)
    
    /// Information needed to reload a section
    public struct ReloadSectionInfo: Hashable {
        
        /// The index of the section in the table
        public let section: Int
        
        /// The row count after the section reload.
        public let updatedRowCount: Int
        
        public func hash(into hasher: inout Hasher) {
            // only use the section to hash. We only want to include one reload info for each section.
            return hasher.combine(section)
        }
    }
    
    /// Static factory for converting an array operation into a table operation
    /// - Parameter arrayOperation: the array operation representing the changes made in a particular section
    /// - Parameter section: the section the operation occurred in
    public static func from<T>(arrayOperation: ArrayOperation<T>, section: Int) -> TableOperation {
        switch arrayOperation {
        case let .update(array):
            return .reloadSections(infos: Set([ReloadSectionInfo(section: section, updatedRowCount: array.count)]))
            
        case let .insert(_, indices):
            return .insertRows(rows: Set(indices.map { IndexPath(row: $0, section: section) }))
            
        case let .remove(_, indices):
            return .deleteRows(rows: Set(indices.map { IndexPath(row: $0, section: section) }))
            
        case let .replace(_, index, removeCount, insertCount):
            let deletedRows = Set((index ..< index + removeCount).map { IndexPath(row: $0, section: section) })
            let insertedRows = Set((index ..< index + insertCount).map { IndexPath(row: $0, section: section) })
            return .replaceRows(deletedRows: deletedRows, insertedRows: insertedRows)
            
        case let .move(_, source, destination):
            return .moveRow(source: IndexPath(row: source, section: section), destination: IndexPath(row: destination, section: section))
        }
    }
}

// MARK: - UITableView

extension UITableView {
    
    /// Updates the table according to the operation that was performed on the data in one of the sections
    /// - Parameter operation: the operation performed in one of the table sections
    /// - Parameter section: the section where the operation occurred
    /// - Parameter animation: animation used during the update
    public func perform<T>(operation: ArrayOperation<T>, section: Int, animation: UITableView.RowAnimation = .automatic) {
        let tableOperation = TableOperation.from(arrayOperation: operation, section: section)
        perform(operation: tableOperation, animation: animation)
    }
    
    /// Updates the table according to the operation that was performed on the backing data
    /// - Parameter operation: operation to perform on the table
    /// - Parameter animation: animation used during the update
    public func perform(operation: TableOperation, animation: UITableView.RowAnimation = .automatic) {
        switch operation {
        case let .reloadTable(updatedSectionCount):
            let updates = { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                
                // delete all existing sections
                let previousSectionCount = strongSelf.numberOfSections
                strongSelf.deleteSections(IndexSet([Int](0 ..< previousSectionCount)), with: animation)
                
                // insert all new sections
                strongSelf.insertSections(IndexSet([Int](0 ..< updatedSectionCount)), with: animation)
            }
            
            performBatchUpdates(updates, completion: nil)
            
        case let .insertSections(sections):
            insertSections(sections, with: animation)

        case let .deleteSections(sections):
            deleteSections(sections, with: animation)
            
        case let .reloadSections(infos):
            let updates = { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                
                for info in infos {
                    // delete all previous rows
                    let previousRowCount = strongSelf.numberOfRows(inSection: info.section)
                    let pathsToDelete = (0 ..< previousRowCount).map { IndexPath(row: $0, section: info.section) }
                    strongSelf.deleteRows(at: pathsToDelete, with: animation)
                    
                    // insert the new rows
                    let pathsToInsert = (0 ..< info.updatedRowCount).map { IndexPath(row: $0, section: info.section) }
                    strongSelf.insertRows(at: pathsToInsert, with: animation)
                }
                
                // then just reload the sections to make sure the header gets updated
                let sections = IndexSet(infos.map { $0.section })
                strongSelf.reloadSections(sections, with: animation)
            }
            
            performBatchUpdates(updates, completion: nil)
            
        case let .insertRows(rows):
            insertRows(at: [IndexPath](rows), with: animation)

        case let .deleteRows(rows):
            deleteRows(at: [IndexPath](rows), with: animation)
            
        case let .replaceRows(deletedRows, insertedRows):
            let updates = { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                
                // delete the rows first since they get processed first
                strongSelf.deleteRows(at: [IndexPath](deletedRows), with: animation)
                
                // insert the new rows
                strongSelf.insertRows(at: [IndexPath](insertedRows), with: animation)
            }
            
            performBatchUpdates(updates, completion: nil)
            
        case let .moveRow(source, destination):
            moveRow(at: source, to: destination)
        }
    }
}
