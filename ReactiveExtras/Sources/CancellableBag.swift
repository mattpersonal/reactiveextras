//
//  CancellableBag.swift
//  ReactiveExtras
//
//  Created by Matthew Richardson on 10/25/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine

/// A bag used for collecting AnyCancellable's for easy cancellation when creating Combine subscriptions.
public final class CancellableBag {
    
    /// the stored cancellables
    private var cancellables = Set<AnyCancellable>()
    
    public init() {}
    
    /// Convenience operator for adding an `AnyCancellable` to a `DisposeBag`. Really, this operator just inserts the `AnyCancellable` into a `Set`.
    /// - Parameter cancellableBag: cancellable bag which will store the cancellable
    /// - Parameter cancellable: the cancellable to add to the cancellable bag
    public static func <<(cancellableBag: inout CancellableBag, cancellable: AnyCancellable) {
        cancellableBag.cancellables.insert(cancellable)
    }
    
    /// Cancel everything in the bag
    public func cancel() {
        cancellables.forEach { $0.cancel() }
        cancellables.removeAll()
    }
    
    deinit {
        cancel()
    }
}
