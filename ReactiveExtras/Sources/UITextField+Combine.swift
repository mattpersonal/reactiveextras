//
//  UITextField+Combine.swift
//  ReactiveExtras
//
//  Created by Matthew Richardson on 10/28/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine

// MARK: - UITextField + Publishers

/// Extending the `UITextField` type to include a publisher for text changed events.
extension UITextField {
    
    /// Supported editing events for publishing
    public enum EditingEventType {
        case editingChanged
        case editingDidBegin
        case editingDidEnd
        case editingDidEndOnExit
    }
    
    /// The event info that gets published when editing events occur
    public struct PublishedEditingEvent {
        public let eventType: EditingEventType
        public let textField: UITextField
    }
    
    /// Creates a publisher that produces editing events
    public final var editingPublisher: EditingPublisher {
        return EditingPublisher(textField: self)
    }
}

// MARK: - UITextField.Subscription

extension UITextField {
    
    /// Custom subscription to capture text changed events.
    final class EditingSubscription<SubscriberType: Subscriber>: Subscription where SubscriberType.Input == PublishedEditingEvent, SubscriberType.Failure == Never {
        private var subscriber: SubscriberType?
        private weak var textField: UITextField?
        
        init(subscriber: SubscriberType, textField: UITextField) {
            self.subscriber = subscriber
            self.textField = textField
            
            textField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
            textField.addTarget(self, action: #selector(editingDidEnd), for: .editingDidEnd)
            textField.addTarget(self, action: #selector(editingDidEndOnExit), for: .editingDidEndOnExit)
            textField.addTarget(self, action: #selector(editingDidBegin), for: .editingDidBegin)
        }
        
        func request(_ demand: Subscribers.Demand) {
            // We do nothing here as we only want to send events when they occur.
            // See, for more info: https://developer.apple.com/documentation/combine/subscribers/demand
        }
        
        func cancel() {
            subscriber = nil
        }
        
        @objc private func editingChanged() {
            guard let textField = textField else {
                return
            }
            
            _ = subscriber?.receive(PublishedEditingEvent(eventType: .editingChanged,
                                                          textField: textField))
        }
        
        @objc private func editingDidEnd() {
            guard let textField = textField else {
                return
            }
            
            _ = subscriber?.receive(PublishedEditingEvent(eventType: .editingDidEnd,
                                                          textField: textField))
        }
        
        @objc private func editingDidEndOnExit() {
            guard let textField = textField else {
                return
            }
            
            _ = subscriber?.receive(PublishedEditingEvent(eventType: .editingDidEndOnExit,
                                                          textField: textField))
        }
        
        @objc private func editingDidBegin() {
            guard let textField = textField else {
                return
            }
            
            _ = subscriber?.receive(PublishedEditingEvent(eventType: .editingDidBegin,
                                                          textField: textField))
        }
    }
}

// MARK: - UITextField.Publishers

extension UITextField {
    
    /// A custom Publisher to produce the text changed events.
    public struct EditingPublisher: Publisher {
        public typealias Output = PublishedEditingEvent
        public typealias Failure = Never
        
        public let textField: UITextField
        
        init(textField: UITextField) {
            self.textField = textField
        }
        
        public func receive<S>(subscriber: S) where S : Subscriber, Failure == S.Failure, Output == S.Input {
            let subscription = EditingSubscription(subscriber: subscriber, textField: textField)
            subscriber.receive(subscription: subscription)
        }
    }
}
