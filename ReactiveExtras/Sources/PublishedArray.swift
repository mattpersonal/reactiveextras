//
//  PublishedArray.swift
//  ReactiveExtras
//
//  Created by Matthew Richardson on 10/22/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine

// MARK: - ArrayOperation

/// The possible operations that could be performed on an array.
public enum ArrayOperation<T> {
    
    /// Operation when the array is updated to a new value.
    case update(array: [T])
    
    /// Operation when elements are inserted into the array.
    case insert(array: [T], indices: Set<Int>)
    
    /// Operation when elements are removed from the array.
    case remove(array: [T], indices: Set<Int>)
    
    /// Operation when elements are replaced in the array.
    /// The deleted rows are processed before the inserted rows.
    case replace(array: [T], index: Int, removeCount: Int, insertCount: Int)
    
    /// Operation when an element is moved in the list
    case move(array: [T], source: Int, destination: Int)
}

// MARK: - PublishedArray

public final class PublishedArray<T>: Publisher {
    public typealias Output = ArrayOperation<T>
    public typealias Failure = Never
    
    /// Convenience type for the subscriber that recieves the array operations that get published by this array
    public typealias AnySubscriberType = AnySubscriber<ArrayOperation<T>, Never>
    typealias SubscriptionType = PublishedArraySubscription<T>
    
    /// Dispatch Queue used to syncronize write access to internals
    /// - Warning: since this queue is `concurrent`, be sure to use the `.barrier` flag whenever mutating the object.
    private let internalQueue = DispatchQueue(label: "PublishedArray internal queue", qos: .userInitiated, attributes: .concurrent)
    
    /// The subscriptions used to notify subscribers when operations are performed on the array.
    private var subscriptionsUnsafe = [SubscriptionType]()
    
    // MARK: - Collection Getters
    
    /// The stored elements.
    /// - Note: mutating the returned array will have no effect on the stored array and will not publish any events. Hence the `readOnly` appended to the name.
    public var elementsReadyOnly: [T] {
        return internalQueue.sync { elementsUnsafe }
    }
    
    /// Backing array for the stored elements.
    /// - Warning: only access this on the background queue.
    private var elementsUnsafe = [T]()
    
    public subscript(position: Int) -> T {
        get {
            return internalQueue.sync { elementsUnsafe[position] }
        }
        
        set {
            replaceCore(replacePosition: position, removeCount: 1, insertElements: [newValue])
        }
    }
    
    public var first: T? {
        return internalQueue.sync { elementsUnsafe.first }
    }
    
    public var last: T? {
        return internalQueue.sync { elementsUnsafe.last }
    }
    
    public var startIndex: Int {
        return internalQueue.sync { elementsUnsafe.startIndex }
    }
    
    public var endIndex: Int {
        return internalQueue.sync { elementsUnsafe.endIndex }
    }
    
    public var elementCount: Int {
        return internalQueue.sync { elementsUnsafe.count }
    }
    
    public var isEmpty: Bool {
        return elementCount == 0
    }
    
    // MARK: - Initialization
    
    public init(_ elements: [T] = []) {
        elementsUnsafe = elements
    }
    
    // MARK: - Publisher Implementation
    
    public func receive<S>(subscriber: S) where S : Subscriber, Never == S.Failure, ArrayOperation<T> == S.Input {
        internalQueue.async(flags: .barrier) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            // create subscription
            let subscription = PublishedArraySubscription(subscriber: AnySubscriber(subscriber), publishedArray: strongSelf)
            
            // store subscription to be used when array operations are published
            strongSelf.subscriptionsUnsafe.append(subscription)
            
            // add the subscription to the subscriber. This allows the subscription to be cancelled later.
            subscriber.receive(subscription: subscription)
            
            // send the initial array value to the subscriber
            _ = subscriber.receive(.update(array: strongSelf.elementsUnsafe))
        }
    }
    
    func cancelledSubscription(_ subscription: SubscriptionType) {
        internalQueue.async(flags: .barrier) { [weak self] in
            self?.subscriptionsUnsafe.removeAll { $0.combineIdentifier == subscription.combineIdentifier }
        }
    }
    
    // MARK: - Collection Functions
    
    public func update(_ array: [T]) {
        updateCore(newArray: array)
    }
    
    public func append(_ newElement: T) {
        insertCore(insertPosition: elementCount, elements: [newElement])
    }
    
    public func append(contentsOf array: [T]) {
        insertCore(insertPosition: elementCount, elements: array)
    }
    
    public func insert(_ newElement: T, at i: Int) {
        insertCore(insertPosition: i, elements: [newElement])
    }
    
    public func insert(contentsOf array: [T], at i: Int) {
        insertCore(insertPosition: i, elements: array)
    }
    
    public func remove(at i: Int) {
        removeCore(removeRange: i ..< i + 1)
    }
    
    public func removeSubrange(_ bounds: Range<Int>) {
        removeCore(removeRange: bounds)
    }
    
    public func removeLast() {
        // store count locally so we don't have to keep running on the internal queue
        let elementCount = self.elementCount
        
        guard elementCount > 0 else {
            return
        }
        
        removeCore(removeRange: elementCount - 1 ..< elementCount)
    }
    
    public func removeFirst() {
        // store count locally so we don't have to keep running on the internal queue
        let elementCount = self.elementCount
        
        guard elementCount > 0 else {
            return
        }
        
        removeCore(removeRange: 0 ..< 1)
    }
    
    public func removeAll() {
        // store count locally so we don't have to keep running on the internal queue
        let elementCount = self.elementCount
        
        guard elementCount > 0 else {
            return
        }
        
        removeCore(removeRange: 0 ..< elementCount)
    }
    
    /// Moves the element at the source position to the destination position
    /// - Parameter source: position of the element to be moved
    /// - Parameter destination: the final position of the element after the move
    /// - Parameter publishOperation: true if the operation should be published. (false if the element was moved in a UITableView and we are just updating the data model to match the changes made in the UI)
    public func moveRow(source: Int, destination: Int, publishOperation: Bool) {
        moveCore(source: source, destination: destination, publishOperation: publishOperation)
    }
    
    public func replaceSubrange(_ range: Range<Int>, with array: [T]) {
        replaceCore(replacePosition: range.lowerBound, removeCount: range.count, insertElements: array)
    }
    
    public func index(after i: Int) -> Int {
        return internalQueue.sync { elementsUnsafe.index(after: i) }
    }
    
    // MARK: - Private Helpers
    
    private func updateCore(newArray: [T]) {
        internalQueue.async(flags: .barrier) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.elementsUnsafe = newArray
            
            strongSelf.unsafePublish(operation: .update(array: newArray))
        }
    }
    
    private func insertCore(insertPosition: Int, elements: [T]) {
        internalQueue.async(flags: .barrier) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.elementsUnsafe.insert(contentsOf: elements, at: insertPosition)
            let insertedRange = insertPosition ..< (insertPosition + elements.count)
            
            strongSelf.unsafePublish(operation: .insert(array: strongSelf.elementsUnsafe, indices: Set(insertedRange)))
        }
    }
    
    private func removeCore(removeRange: Range<Int>) {
        internalQueue.async(flags: .barrier) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.elementsUnsafe.removeSubrange(removeRange)

            strongSelf.unsafePublish(operation: .remove(array: strongSelf.elementsUnsafe, indices: Set(removeRange)))
        }
    }
    
    private func replaceCore(replacePosition: Int, removeCount: Int, insertElements: [T]) {
        internalQueue.async(flags: .barrier) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.elementsUnsafe.removeSubrange(replacePosition ..< (replacePosition + removeCount))
            strongSelf.elementsUnsafe.insert(contentsOf: insertElements, at: replacePosition)
            
            strongSelf.unsafePublish(operation: .replace(array: strongSelf.elementsUnsafe, index: replacePosition, removeCount: removeCount, insertCount: insertElements.count))
        }
    }
    
    private func moveCore(source: Int, destination: Int, publishOperation: Bool) {
        internalQueue.async(flags: .barrier) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            let movedElement = strongSelf.elementsUnsafe.remove(at: source)
            strongSelf.elementsUnsafe.insert(movedElement, at: destination)
            
            if publishOperation {
                strongSelf.unsafePublish(operation: .move(array: strongSelf.elementsUnsafe, source: source, destination: destination))
            }
        }
    }
    
    /// Publishes the operation event to the subscribers
    /// - Parameter operation: operation to publish
    /// - Warning: not thread-safe. ensure this is called on the internal queue
    private func unsafePublish(operation: ArrayOperation<T>) {
        for subscription in subscriptionsUnsafe {
            _ = subscription.subscriber?.receive(operation)
        }
    }
}

// MARK: - PublishedArraySubscription

final class PublishedArraySubscription<Element>: Subscription {
    
    /// Subscriber that receives the published events
    var subscriber: AnySubscriber<ArrayOperation<Element>, Never>?
    private weak var publishedArray: PublishedArray<Element>?
        
    init(subscriber: AnySubscriber<ArrayOperation<Element>, Never>, publishedArray: PublishedArray<Element>) {
        self.subscriber = subscriber
        self.publishedArray = publishedArray
    }
    
    func request(_ demand: Subscribers.Demand) {
        // TODO: consider handling the demand requested. For now, just always forward the events since that will handle almost all of my use cases. Honestly, not 100% when this gets called in our scenarios. 
    }
    
    func cancel() {
        subscriber = nil
        publishedArray?.cancelledSubscription(self)
    }
}
