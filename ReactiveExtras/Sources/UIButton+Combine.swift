//
//  UIButton+Combine.swift
//  ReactiveExtras
//
//  Created by Matthew Richardson on 10/25/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine

extension UIButton {
    
    /// Publisher that produces events whenever the button is tapped
    public var tapPublisher: AnyPublisher<UIButton, Never> {
        let mappedPublisher = eventPublisher(for: .touchUpInside).map { control -> UIButton in
            guard let button = control as? UIButton else {
                fatalError("The control returned should always be a UIButton.")
            }
            
            return button
        }
        
        return AnyPublisher(mappedPublisher)
    }
}

