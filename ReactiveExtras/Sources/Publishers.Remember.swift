//
//  Publishers.Remember.swift
//  ReactiveExtras
//
//  Created by Matthew Richardson on 10/28/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine

// MARK: - Publisher + remember

extension Publisher {
    
    /// Produces a publisher that includes some information about previous events. 
    public func remember() -> Publishers.Remember<Self> {
        return Publishers.Remember(upstream: self)
    }
}

// MARK: - Publishers.History

extension Publishers {
    
    /// Publisher that includes the change history with every published event
    public struct Remember<Upstream: Publisher>: Publisher {
        
        public typealias Failure = Upstream.Failure
        public typealias Output = Event<Upstream.Output>
        
        /// The publisher from which this publisher receives elements.
        public let upstream: Upstream
        
        public init(upstream: Upstream) {
            self.upstream = upstream
        }
        
        public func receive<Downstream: Subscriber>(subscriber: Downstream) where Failure == Downstream.Failure, Output == Downstream.Input {
            upstream.subscribe(Inner(downstream: subscriber))
        }
    }
}

// MARK: - Publishers.Remember.Event

extension Publishers.Remember {
    
    /// The output event information for the Remember publisher.
    public struct Event<T> {
        
        /// Current event value. There will always be a current event.
        public let current: T
        
        /// History about previous events. History is nil for the first event.
        public let history: History<T>?
    }
}

// MARK: - Publishers.Remember.History

extension Publishers.Remember {
    
    /// The change information provided with every published event
    public struct History<T> {
        
        /// The previous event
        public let previous: T
    }
}

// MARK: - Publishers.History.Inner

extension Publishers.Remember {
    
    private final class Inner<Downstream: Subscriber>: Subscriber where Downstream.Input == Output, Downstream.Failure == Failure {
        
        public typealias Input = Upstream.Output
        public typealias Failure = Upstream.Failure
        
        public let combineIdentifier = CombineIdentifier()
        
        private let downstream: Downstream
        
        /// We cache the previous input so that we can forward it along with the new event.
        private var history: History<Input>?
        
        fileprivate init(downstream: Downstream) {
            self.downstream = downstream
        }
        
        public func receive(subscription: Subscription) {
            downstream.receive(subscription: subscription)
        }
        
        public func receive(_ input: Upstream.Output) -> Subscribers.Demand {
            let demand = downstream.receive(Event(current: input, history: history))
            
            // update the history
            history = History(previous: input)
            
            return demand
        }
        
        public func receive(completion: Subscribers.Completion<Upstream.Failure>) {
            downstream.receive(completion: completion)
        }
    }
}
