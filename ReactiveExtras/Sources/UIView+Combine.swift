//
//  UIView+Combine.swift
//  ReactiveExtras
//
//  Created by Matthew Richardson on 10/30/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine

extension UIView {
    
    /// Publisher producing events whenever tap gestures are recognized on the view.
    /// - Note: calling this does not modify the isUserInteractionEnabled property on the view. The caller must continue to manage that setting.
    public var tapGesturePublisher: TapGesturePublisher {
        return TapGesturePublisher(view: self)
    }
}

extension UIView {
    
    final class TapGestureSubscription<SubscriberType: Subscriber>: Subscription where SubscriberType.Input == UIView, SubscriberType.Failure == Never {
        
        private var subscriber: SubscriberType?
        private weak var view: UIView?
        
        init(subscriber: SubscriberType, view: UIView) {
            self.subscriber = subscriber
            self.view = view
            
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped)))
        }
        
        func request(_ demand: Subscribers.Demand) {
            // We do nothing here as we only want to send events when they occur.
            // See, for more info: https://developer.apple.com/documentation/combine/subscribers/demand
        }
        
        func cancel() {
            subscriber = nil
        }
        
        @objc private func viewTapped() {
            if let view = view {
                _ = subscriber?.receive(view)
            }
        }
    }
}

extension UIView {
    
    public struct TapGesturePublisher: Publisher {
        public typealias Output = UIView
        public typealias Failure = Never
        
        public let view: UIView
        
        init(view: UIView) {
            self.view = view
        }
        
        public func receive<S>(subscriber: S) where S : Subscriber, Failure == S.Failure, Output == S.Input {
            let subscription = TapGestureSubscription(subscriber: subscriber, view: view)
            subscriber.receive(subscription: subscription)
        }
    }
}
