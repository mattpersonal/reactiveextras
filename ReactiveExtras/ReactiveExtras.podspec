Pod::Spec.new do |s|
s.name             = 'ReactiveExtras'
s.version          = '0.0.1'
s.summary          = 'Framework including custom reactive objects that extend the ReactiveCocoa and ReactiveSwift frameworks.'

s.description      = <<-DESC
'Framework including custom reactive objects that extend the ReactiveCocoa and ReactiveSwift frameworks.'
DESC

s.homepage         = 'https://bitbucket.org/mattpersonal/reactiveextras'
s.license          = { :type => 'MIT', :file => 'ReactiveExtras/LICENSE' }
s.author           = { 'mrrichardson52' => 'mattyrrich52@gmail.com' }
s.source           = { :git => 'https://mrrichardson52@bitbucket.org/mattpersonal/reactiveextras.git', :tag => s.version.to_s }

s.ios.deployment_target = '12.2'

s.source_files = 'ReactiveExtras/ReactiveExtras/Sources/**/*'

s.swift_version= '4.2'

s.dependency 'ReactiveCocoa'

end
