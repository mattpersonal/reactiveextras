//
//  ObservableArrayTests.swift
//  ReactiveExtrasTests
//
//  Created by Matthew Richardson on 9/12/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest
@testable import ReactiveExtras

class ObservableArrayTests: XCTestCase {
    
    func test_signal_update() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [1, 2, 3]
        let updateArray = [4, 5, 6]
        let expectedArray = updateArray
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .update(array):
                XCTAssert(array == expectedArray)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.update(updateArray)
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_insert_element() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [1, 2, 4]
        let insertElement = 3
        let expectedArray = [1, 2, 3, 4]
        let insertPosition = 2
        let expectedInsertRange = insertPosition ..< (insertPosition + 1)
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .insert(array, insertRange):
                XCTAssert(array == expectedArray)
                XCTAssert(insertRange == expectedInsertRange)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.insert(insertElement, at: insertPosition)
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_insert_array() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [1, 5]
        let insertArray = [2, 3, 4]
        let expectedArray = [1, 2, 3, 4, 5]
        let insertPosition = 1
        let expectedInsertRange = insertPosition ..< (insertPosition + insertArray.count)
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .insert(array, insertRange):
                XCTAssert(array == expectedArray)
                XCTAssert(insertRange == expectedInsertRange)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.insert(contentsOf: insertArray, at: insertPosition)
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_append_element() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [1, 2, 3]
        let appendElement = 4
        let expectedArray = [1, 2, 3, 4]
        let expectedInsertRange = initialArray.count ..< (initialArray.count + 1)
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .insert(array, insertRange):
                XCTAssert(array == expectedArray)
                XCTAssert(insertRange == expectedInsertRange)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.append(appendElement)
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_append_array() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [1, 2]
        let appendArray = [3, 4, 5]
        let expectedArray = [1, 2, 3, 4, 5]
        let expectedInsertRange = initialArray.count ..< (initialArray.count + appendArray.count)
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .insert(array, insertRange):
                XCTAssert(array == expectedArray)
                XCTAssert(insertRange == expectedInsertRange)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.append(contentsOf: appendArray)
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_subscript() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [1, 999, 3]
        let replacementElement = 2
        let replacementPosition = 1
        
        let expectedReplaceStartIndex = replacementPosition
        let expectedRemovedCount = 1
        let expectedInsertedCount = 1
        let expectedArray = [1, 2, 3]
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .replace(array, replaceStartIndex, removedCount, insertCount):
                XCTAssert(array == expectedArray)
                XCTAssert(replaceStartIndex == expectedReplaceStartIndex)
                XCTAssert(removedCount == expectedRemovedCount)
                XCTAssert(insertCount == expectedInsertedCount)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray[replacementPosition] = replacementElement
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_replace() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [1, 999, 999, 999, 4]
        let replacementArray = [2, 3]
        let replacementPosition = 1
        let removeRange = replacementPosition ..< replacementPosition + 3
        
        let expectedReplaceStartIndex = replacementPosition
        let expectedRemovedCount = 3
        let expectedInsertedCount = 2
        let expectedArray = [1, 2, 3, 4]
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .replace(array, replaceStartIndex, removedCount, insertCount):
                XCTAssert(array == expectedArray)
                XCTAssert(replaceStartIndex == expectedReplaceStartIndex)
                XCTAssert(removedCount == expectedRemovedCount)
                XCTAssert(insertCount == expectedInsertedCount)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.replaceSubrange(removeRange, with: replacementArray)
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_remove() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [1, 2, 999, 3]
        let removePosition = 2
        let expectedArray = [1, 2, 3]
        let expectedRemoveRange = removePosition ..< removePosition + 1
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .remove(array, removeRange):
                XCTAssert(array == expectedArray)
                XCTAssert(removeRange == expectedRemoveRange)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.remove(at: removePosition)
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_removeSubrange() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [1, 2, 999, 999, 3]
        let removeRange = 2 ..< 4
        let expectedArray = [1, 2, 3]
        let expectedRemoveRange = removeRange
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .remove(array, removeRange):
                XCTAssert(array == expectedArray)
                XCTAssert(removeRange == expectedRemoveRange)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.removeSubrange(removeRange)
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_removeLast() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [1, 2, 3, 999]
        let expectedArray = [1, 2, 3]
        let expectedRemoveRange = initialArray.count - 1 ..< initialArray.count
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .remove(array, removeRange):
                XCTAssert(array == expectedArray)
                XCTAssert(removeRange == expectedRemoveRange)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.removeLast()
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_removeFirst() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [999, 1, 2, 3]
        let expectedArray = [1, 2, 3]
        let expectedRemoveRange = 0 ..< 1
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .remove(array, removeRange):
                XCTAssert(array == expectedArray)
                XCTAssert(removeRange == expectedRemoveRange)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.removeFirst()
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signal_removeAll() {
        let testExpectation = expectation(description: "expectation that the test completes")
        
        let initialArray = [999, 999, 999, 999]
        let expectedArray = [Int]()
        let expectedRemoveRange = 0 ..< initialArray.count
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.signal.observeValues { operation in
            switch operation {
            case let .remove(array, removeRange):
                XCTAssert(array == expectedArray)
                XCTAssert(removeRange == expectedRemoveRange)
                
            default:
                XCTFail("Expected update operation")
            }
            
            testExpectation.fulfill()
        }
        
        // trigger observations
        observableArray.removeAll()
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_signalProducer() {
        let updateExpectation = expectation(description: "expectation that the update operation is handled")
        let insertExpectation = expectation(description: "expectation that the insert operation is handled")
        
        let initialArray = [1, 2, 3]
        let expectedArrayAfterUpdate = initialArray
        
        let appendElement = 4
        let expectedArrayAfterInsert = [1, 2, 3, 4]
        let expectedInsertRange = initialArray.count ..< initialArray.count + 1
        
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        // setup observer
        observableArray.producer.startWithValues { operation in
            switch operation {
            case let .update(array):
                XCTAssert(array == expectedArrayAfterUpdate)
                updateExpectation.fulfill()
                
            case let .insert(array, insertRange):
                XCTAssert(array == expectedArrayAfterInsert)
                XCTAssert(insertRange == expectedInsertRange)
                insertExpectation.fulfill()
                
            default:
                XCTFail("Expected update operation")
            }
        }
        
        // trigger observations
        observableArray.append(appendElement)
        
        let result = XCTWaiter.wait(for: [updateExpectation, insertExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
    }
    
    func test_count() {
        let observableArray = ObservableArray<Int>(array: [1, 2, 3])
        XCTAssert(observableArray.count == 3)
    }
    
    func test_first() {
        let observableArray = ObservableArray<Int>(array: [1, 2, 3])
        XCTAssert(observableArray.first == 1)
    }
    
    func test_last() {
        let observableArray = ObservableArray<Int>(array: [1, 2, 3])
        XCTAssert(observableArray.last == 3)
    }
    
    func test_loop() {
        let initialArray = [0, 1, 2, 3]
        let observableArray = ObservableArray<Int>(array: initialArray)

        var arrayToBuild = [Int]()
        
        for value in observableArray {
            arrayToBuild.append(value)
        }
        
        XCTAssert(arrayToBuild == initialArray)
    }
    
    func test_toArray() {
        let initialArray = [0, 1, 2, 3]
        let observableArray = ObservableArray<Int>(array: initialArray)
        
        XCTAssert(observableArray.toArray() == initialArray)
    }
}
