//
//  ObservableTableTests.swift
//  ReactiveExtrasTests
//
//  Created by Matthew Richardson on 9/12/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest
@testable import ReactiveExtras

class ObservableTableTests: XCTestCase {
    
    func test_getSection() {
        let section0 = ObservableArray(array: [1])
        let section1 = ObservableArray(array: [2])
        let observableTable = ObservableTable(sections: [section0, section1])
    }
    
}
