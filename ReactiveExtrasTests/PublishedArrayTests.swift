//
//  PublishedArrayTests.swift
//  ReactiveExtrasTests
//
//  Created by Matthew Richardson on 10/22/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest
@testable import ReactiveExtras

class PublishedArrayTests: XCTestCase {
    
    func test_update() {
        let testExpectation = expectation(description: "Test expectation that the test completes")
        
        let initialArray = [0, 1, 2]
        let updatedArray = [3, 4, 5]
        
        let publishedArray = PublishedArray(initialArray)
        
        var operationReceivedCount = 0
        
        let cancellable = publishedArray.sink { operation in
            switch operation {
            case let .update(array):
                operationReceivedCount += 1
                
                if operationReceivedCount == 1 {
                    XCTAssert(array == initialArray)
                    
                } else if operationReceivedCount == 2 {
                    XCTAssert(array == updatedArray)
                    testExpectation.fulfill()

                } else {
                    XCTFail("Shouldn't have received more than 2 operations")
                    testExpectation.fulfill()
                }
                
            default:
                XCTFail()
                testExpectation.fulfill()
            }
        }
        
        publishedArray.update(updatedArray)
        
        let result = XCTWaiter.wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled")
        cancellable.cancel()
    }
    
    func test_insert() {
        
    }
    
    func test_remove() {
        
    }
    
    func test_replace() {
        
    }
}
