//
//  PublishersHistoryTests.swift
//  ReactiveExtrasTests
//
//  Created by Matthew Richardson on 10/28/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest
import Combine
@testable import ReactiveExtras

class PublishersHistoryTests: XCTestCase {
    
    @Published var publishedInteger: Int?
    
    private var cancellable: Cancellable?
    
    override func setUp() {
        publishedInteger = nil
    }
    
    override func tearDown() {
        cancellable?.cancel()
        cancellable = nil
    }
    
    func test_history() {
        let testExpectation = expectation(description: "test expectation")
        
        var eventCount = 0
        
        cancellable = $publishedInteger
            .remember()
            .sink { event in
                switch eventCount {
                case 0:
                    XCTAssert(event.history == nil)
                    XCTAssert(event.current == nil)
                    
                case 1:
                    XCTAssert(event.history?.previous == nil)
                    XCTAssert(event.current == 0)
                    
                case 2:
                    XCTAssert(event.history?.previous == 0)
                    XCTAssert(event.current == 1)
                    testExpectation.fulfill()
                    
                default:
                    XCTFail("Only expected 3 events")
                }
                
                eventCount += 1
        }
        
        publishedInteger = 0
        publishedInteger = 1
        
        let result = XCTWaiter().wait(for: [testExpectation], timeout: 3)
        XCTAssert(result == .completed, "Expectation not fulfilled.")
    }
}
